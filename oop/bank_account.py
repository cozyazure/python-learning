class BankAccount:
    # constructor
    def __init__(self, owner=None, initial_balance=0):
        self.owner = owner
        self.balance = initial_balance
        # base interest rate
        self.interest = 0.0002

    def get_interest(self):
        return self.interest

    def deposit(self, deposit_amount):
        self.balance += deposit_amount

    def withdrawal(self, withdrawal_amount):
        if withdrawal_amount > self.balance:
            raise Exception('Not enough money to withdraw')
        self.balance -= withdrawal_amount

    def read_balance(self):
        return self.balance

    def accrue_interest(self):
        self.balance *= 1 + self.interest


class HighInterestAccount(BankAccount):
    def __init__(self, owner=None, balance=0):
        super().__init__(owner, balance)
        self.interest = 0.02


class CurrentAccount(BankAccount):
    def withdrawal(self, withdrawal_amount):
        self.balance -= withdrawal_amount
