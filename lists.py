shopping_list = ['apple', 'banana', 'peanut butter', 'jam', 'toilet paper']

# accessor, steps
# remember this is zero-th index, so first item is 0
print(shopping_list[0])  # apple
print(shopping_list[4])  # toilet paper

# add to list
shopping_list.append('soy sauce')
print(shopping_list)  # ['apple', 'banana', 'peanut butter', 'jam', 'toilet paper', 'soy sauce']

# remove from list using pop()
# pop() remove the last item of the list and return the removed items
removed = shopping_list.pop()
print(removed)  # soy sauce
print(shopping_list)  # ['apple', 'banana', 'peanut butter', 'jam', 'toilet paper']

# for loop
for item in shopping_list:
    print(item)

# count / length of list
print(f'Total numbers to buy in shopping_list is {len(shopping_list)}')

# list comprehension
number_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

doubled = [x * 2 for x in number_list]  # [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
even = [x for x in number_list if x % 2 == 0]  # [2, 4, 6, 8, 10]
square_if_odd = [x * x for x in number_list if x % 2 != 0]  # [1, 9, 25, 49, 81]

# lists can take in any types, but be careful when dealing with them
mixed_lists = ['I am a string', 1, 2.5, 3, 4, 5, 8.5, 10, True]
filtered = [x for x in mixed_lists if type(x).__name__ == 'int' and x >= 5]
print(filtered)  # gives [5, 10]
