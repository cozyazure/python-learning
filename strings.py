phrase = 'Hello World'
print(phrase)

# strings are actually lists of characters
print(phrase[0])  # gives H

# take every odd number position of the string
print(phrase[1::2])  # gives 'el ol'

# string concatenation
word1 = 'How'
word2 = 'are you?'
space = ' '
print(word1 + space + word2)

# string formatting
temperature = 36.9
print(f'Your temperature is {temperature} degree celcius')

# find words in string
sentence = 'The brown fox jump over the lazy dog'
word = 'jump'
if word in sentence:
    print(f'the word {word} is found!')

# newlines, tabs and other whitespaces
print('This is first sentence \nThis is second sentence')
print('\t This is a new paragraph')
print('Anything before will not be print\rCarriage return will ignore anything before')

# stripping
data = '         There is a lot of whitespace before and after          '
print(data.strip())

# escaping
print('I want to print backslashes \\')
print('I want to print inverted comma \'')
