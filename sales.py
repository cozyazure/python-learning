import pandas as pd

# configure the display of pd for easier debugging
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# opening of a csv file using pandas
df = pd.read_csv('sales_numbers.csv')

# select columns you want
simple_version = df[['Country', 'ItemType', 'UnitsSold', 'UnitPrice', 'UnitCost']]

# add new column
df['TotalPrice'] = df['UnitPrice'] * df['UnitsSold']
df['TotalCost'] = df['UnitCost'] * df['UnitsSold']
df['TotalRevenue'] = df['TotalPrice'] - df['TotalCost']
# quiz how to do it in one line?
df['TotalRevenue2'] = (df['UnitPrice'] - df['UnitCost']) * df['UnitsSold']
# do it in using lambda functions
# need to add axis = 1 to tell pandas that u are changing rows instead of columns
df['TotalRevenue3'] = df.apply(lambda row: (row['UnitPrice'] - row['UnitCost']) * row['UnitsSold'], axis=1)

# filtering
filtered = df[df['Country'] == 'Benin']

# quiz can you filter rows where items are sold in Malaysia with price> 1M?
malaysia_mil = df[(df['Country'] == 'Malaysia') & (df['TotalRevenue'] > 1000000)]

# sort according to country alphabetically
df = df.sort_values('Country')
# you can sort by total revenue too
# you can add 'ascending = False' so that you can see the highest revenue on top
df = df.sort_values('TotalRevenue', ascending=False)

# math operations
# sum
total_revenue = df['TotalRevenue'].sum()
# cumulative sum
df['Cumulative'] = df['TotalRevenue'].cumsum()

# joins
region_df = pd.read_csv('region_mapping.csv')
joined = df.merge(region_df, on='Country', how='left')

# group by one column
grouped_by_region = joined.groupby('Region')['TotalRevenue'].sum().reset_index()
grouped_by_region['TotalRevenue (Mil)'] = grouped_by_region['TotalRevenue'] / 1000 / 1000
# group by multi column
group_by_region_city = joined.groupby(['Region', 'Country'])['TotalRevenue'].sum().reset_index()
# get the max
maximum = joined.groupby(['Region'])['TotalRevenue'].max()
# how to get the other information but with max?
max_with_info = joined[joined['TotalRevenue'] == joined['TotalRevenue'].max()]

# Heavy analysis
# filter by one country only
sg = joined[joined['Country'] == 'Singapore']
sg = sg[['Country', 'ItemType', 'SalesChannel', 'OrderPriority', 'OrderDate', 'OrderID', 'ShipDate', 'UnitsSold',
         'UnitPrice', 'UnitCost', 'TotalPrice', 'TotalCost', 'TotalRevenue']]

# get the monthly sum
sg['OrderDate'] = pd.to_datetime(sg['OrderDate'])
sg['ShipDate'] = pd.to_datetime(sg['ShipDate'])
sg['OrderMonth'] = sg['OrderDate'].dt.month
sg['ShipMonth'] = sg['ShipDate'].dt.month
monthly_max = sg.groupby('OrderMonth')['TotalRevenue'].sum().reset_index()

# is monthly sum truly the best?
# how about increments
monthly_max['monthly_increment'] = monthly_max['TotalRevenue'].diff()
print(monthly_max.sort_values('monthly_increment'))

# what is the revenue efficiency?
sg['DeliveryDays'] = sg['ShipDate'] - sg['OrderDate']
sg['RevenuePerDay'] = sg['TotalRevenue'] / sg['DeliveryDays'].dt.days

# which item has the best revenue effiency?
best_efficiency_item = sg.groupby('ItemType')['RevenuePerDay'].mean().reset_index().sort_values('RevenuePerDay')
print(best_efficiency_item)

# save as a new csv
# use index = False to prevent the ugly column of the index
df.to_csv('results.csv')
