# simple assignment of numeric type
a = 1
print(a)
print(type(a))
print(type(a).__name__)

# math operations
a = 2
b = 2
total = a + b
subtract = a - b
multiply = a * b
division = a / b

# float, decimal
fraction = 3 / 5
print(fraction)
print(type(fraction))  # float
floor = 4 // 3
print(floor)  # gives 1 instead of 1.333

# Quiz
# what is the result of the following?
a = 2
b = 3
c = 5
result = a + b * c
print(result)  # remember bodmus rule! # so if you put bracket first (a+b) * c then result will be 25
