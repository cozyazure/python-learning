def addition(a, b):
    return a + b


total = addition(1, 2)
print(total)  # gives 3


# you can also use default values for the parameters
def calculate_circle_area(radius=1):
    return 3.1412 * radius * radius


default_area = calculate_circle_area()  # gives 3.1412
area = calculate_circle_area(4)  # gives 50.2592

# iterables built in lambda functions
# map function is to transform each and every item in the list
number_list = [1, 2, 3, 4, 5]
mapped_list = map(lambda x: x * 2, number_list)
print(list(mapped_list))  # [2, 4, 6, 8, 10]

# filter function takes in an argument that must return Truthy or Falsey
filtered_list = filter(lambda x: x % 2 == 0, number_list)
print(list(filtered_list))  # [2, 4]


# you can also define your own functions and pass it into the iterables functions
def my_own_filter(x):
    return x > 3 and x % 2 == 0  # if the number is greater than 3 and is even


special_filtered = filter(my_own_filter, number_list)
print(list(special_filtered))  # [4]
