# strictly equal
if 5 == (10 / 2):
    print("5 is equals to 10 divide by 2!")
else:
    print("something is wrong with the math")

# greater or equal to
result = 5 >= 3 / 2
print(result)  # True

# less than
result = 3 < 2
print(result)  # False

# range
x = 5
if 2 < x < 10:
    print("x is in the range of 2 to 10")
else:
    print("x is not in the range of 2 to 10")

# and
x = 10
if x >= 10 and (x % 2 == 0):  # % stands for modulo , it gives the remainder of the division
    print("x is greater than 10 and is even")

# or
x = 5
if x < 4 or (x % 2 == 1):
    print("x is either less than 4 or odd")
